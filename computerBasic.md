## Computer Basic

### Resources

- https://edu.gcfglobal.org/en/computerbasics/
- https://www.britannica.com/technology/computer
- https://www.mac-history.net/apple-history-2/apple-i/2012-07-08/apple-i
- books
  - 浪潮之巅（吴军著）

## Class One

### before class
- watch video: what is a computer https://edu.gcfglobal.org/en/computerbasics/what-is-a-computer/1/ 

### Types of Computer
- Supercomputer
- Mainframe
- worksation
- micro computer
- mini computer

### computer categories
- desktop, laptop, tablet, mobile phone
  - laptop: all-in-one design, a built-in monitor, keyboard, touchpad, and speakers
  - mobile phone: OS (Android, Mac iOS)

### hardware of computer
- https://www.crucial.com/usa/en/what-is-computer-hardware
- motherboard(主板)，graphics card，CPU(Central Processing Unit), RAM/memory, hard disk
- input: keyboard, mouse/trackball/touchpad;  output: monitor

#### Moore’s Law (摩尔定律)

### software of computer
- Operating System
  - It manages the computer's memory and processes, as well as all of its software and hardware.
  - common OS types of PC: Microsoft Windows, Mac OS X and Linux
    - desktop OS version market share: http://gs.statcounter.com/os-market-share/desktop/worldwide/2019  
      windows: 77.5%, Unknown: 13%, OS X: 7.8%, Linux: 1.6% 

- application (APP)
  - desktop app: Word processors, Web browsers(IE, Mozilla Firefox, Google Chrome, and Safari), Media players, Games, photoshop
  - business app: database
  - web app
  - mobile apps: Gmail, Instagram, Weixin, QQ

### two main styles of desktop computers
- PC
  - IBM PC 5150 on 1981  
    <img src="gif/IBM_PC_5150-780x564.jpg" width="250">
  - IBM PC Compatible
  - typically includes Microsoft Windows (PC-DOS at early ages)
  - companies invloved: IBM, Dell, Lenovo, acer, Sumsung, Sony, Toshiba

- Macs
  - Apple I on 1976  
    <img src="gif/1976_Apple_I_Computer_in_A_homemade_woodencase.jpg" width="250">

  - Lisa on 1983 is the first widely sold PC with a GUI (graphical user interface)  
    original inventor of GUI is Xerox PARC
  - always use the MacOS

### after class
- watch video about Apple I: https://www.mac-history.net/apple-history-2/apple-i/2012-07-08/apple-i
- EXE: check hardware on Mac
- EXE: search and read about the war between Apple and IBM 

### Class Two

- file system
  
- Internet
  - connecting to the Internet: modem, router
  - using Internet: website, web browser, email, social media
  - Internet safety, Internet basic, better search

- Cloud
  - file storage, file sharing, backing up data

- Protecting your computer

- trouble shooting
  - an application is slow or frozen
  - all applications are slow or frozen

- learning a new program
  - Look for similarities with programs you've used
  - Check for hidden toolbars or panels
  - find Tutorial/QuickStart on the Internet
  - If you're having trouble
    1. Use the help feature: try safari
    1. google it!

- Data Center

### basic computer skill
- https://blog.connectionsacademy.com/The-Student-s-Computer-Maintenance-Checklist/











## 编程
- Python图形编程 (Python Turtle Graphics)： https://mp.weixin.qq.com/s?__biz=MzA3MDA2MDczMA==&mid=2651573004&idx=2&sn=3d4c63596fab85d229dde61fe81e86f2&chksm=853df4d9b24a7dcff44d29f7286873966f8306bdacf359889b99aca6f8661e868b74157028f1&mpshare=1&scene=1&srcid=&pass_ticket=SnLFae%2BGbzNp9TsHh6w6wjcS9HBJOkizcNwgK6YoN5iRvM7Wd8NEXd2WBqEhKM5N#rd


## IT
### digital certificates and CA
- 
### HTTPS and SSL/TLS
- https://program-think.blogspot.com/2014/11/https-ssl-tls-2.html